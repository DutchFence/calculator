const turnOffEnergy = (energy, ...buttons) => buttons.forEach((x) => x.disabled = energy);
const findDecimals = (answer) => {
    let decimalIndex = answer.toString().indexOf(".");
    if (decimalIndex === 0) return;
    return answer.toString().slice(decimalIndex + 1);
};
const countChar = (str, char) => {
    const expr = new RegExp(char, "g");
    const count = String(str)?.match(expr)?.length ?? 0;
    return count;
};