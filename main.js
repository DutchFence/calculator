window.addEventListener("load", () => {
    'use strict';
    // Elements
    const screen = document.querySelector("input");

    const btnLeft = document.querySelector("#left");
    const btnRight = document.querySelector("#right");

    const btnOn = document.querySelector("#on");
    const btnOff = document.querySelector("#off");
    const btnAns = document.querySelector("#ans");
    const btnDel = document.querySelector("#del");
    const btnEqu = document.querySelector("#equate");
    const btnClear = document.querySelector("#clear");
    const btnDot = document.querySelector("#dot");
    const btnOpen = document.querySelector("#open");
    const btnClose = document.querySelector("#close");

    const intButtons = document.querySelectorAll(".integer");
    const opButtons = document.querySelectorAll(".unique");

    // Global Variables (global within load)
    const answers = [];
    let dotAllowed = true;
    const buttons = [btnAns, btnDel, btnClear, btnDot, btnEqu, btnLeft, btnRight, btnClose, btnOpen, ...opButtons, ...intButtons];
    let i = 0;

    // TURN OFF MACHINE
    turnOffEnergy(true, ...buttons);

    // BUTTON RIGHT
    btnRight.addEventListener("mousedown", (e) => {
        if (e.type === "mousedown") screen.scrollLeft += 25;
    });

    // BUTTON LEFT
    btnLeft.addEventListener("mousedown", (e) => {
        if (e.type === "mousedown") screen.scrollLeft -= 25;
    });

    //  INTEGER BUTTONS
    intButtons.forEach((button) => {
        button.addEventListener("click", () => {
            screen.value += button.value;
            if (screen.value.length > 8) {
                screen.scrollLeft += 30;
            }
        });
    });

    // OPERATOR BUTTONS
    opButtons.forEach((button) => {
        button.addEventListener("click", () => {
            let lastChar = screen.value.substring(screen.value.length - 1);
            if (screen.value === "" && button.value !== "-") return;
            switch (lastChar) {
                case "/":
                    break;
                case "+":
                    break;
                case "*":
                    break;
                case "-":
                    break;
                case "(" && button.value !== "-":
                    break;
                default:
                    dotAllowed = true;
                    screen.value += button.value;
                    if (screen.value.length > 8) {
                        screen.scrollLeft += 30;
                    }
                    break;
            }
        });
    });

    // POWER ON BUTTON
    btnOn.addEventListener("click", () => {
        screen.value = "";
        dotAllowed = true;
        turnOffEnergy(false, ...buttons);
        screen.style.background = "linear-gradient(to left, hsla(124, 19%, 84%, 1), hsla(124, 19%, 61%, 1))";
    });

    // POWER OFF BUTTON
    btnOff.addEventListener("click", () => {
        screen.value = "";
        turnOffEnergy(true, ...buttons);
        answers.length = 0;
        screen.style.background = "hsla(140, 14%, 25%, 1)";
    });

    // ANSWER BUTTON
    btnAns.addEventListener("click", () => {
        if (answers.length === 0) return;
        screen.value = answers[i];
        if (screen.value % 1 !== 0) dotAllowed = false;
        i + 2 > answers.length ?
            i = 0 :
            i++;
    });

    // DELETE BUTTON
    btnDel.addEventListener("click", () => {
        if (screen.value.slice(-1) === ".") dotAllowed = true;;
        screen.value = screen.value.slice(0, -1);
    });

    // CLEAR BUTTON
    btnClear.addEventListener("click", () => {
        screen.value = "";
        dotAllowed = true;
    });

    // CALCULATE BUTTON
    btnEqu.addEventListener("click", () => {
        const open = countChar("[(]");
        const close = countChar("[)]");
        if (open > close) return;
        let answer;

        // Prevent syntax Error, try has to be changed to a conditional validator
        try {
            answer = Function(`return ${screen.value};`)();
        }
        catch (error) {
            console.log(error);
            screen.value = "Invalid";
            turnOffEnergy(true, ...buttons);
            setTimeout(() => {
                turnOffEnergy(false, ...buttons);
                screen.value = "";
            }, 2000);
        }

        // Prevent Unexpected Errors
        if (isNaN(answer)) return;

        // Find decimals
        let decimals = findDecimals(answer);
        // If less than 5 decimals , print all decimals. Otherwise scientific notation
        answer = (String(decimals).length <= 5)
            ? answer.toLocaleString(navigator.language, { maximumFractionDigits: 5 })
            : answer.toExponential(6);

        screen.value = answer;
        if (screen.value % 1 !== 0) dotAllowed = false;
        screen.scrollLeft -= 25;
        answers.unshift(answer);
    });

    // DOT BUTTON
    btnDot.addEventListener("click", () => {
        if (dotAllowed === false) return;
        screen.value += btnDot.value;
        if (screen.value.length > 8) {
            screen.scrollLeft += 30;
        }
        dotAllowed = false;
    });

    //OPEN PARENTHESES
    btnOpen.addEventListener("click", () => {
        let lastChar = screen.value.substring(screen.value.length - 1);
        switch (lastChar) {
            case "-":
                screen.value += btnOpen.value;
                if (screen.value.length > 8) {
                    screen.scrollLeft += 30;
                }
                break;
            case "(":
                screen.value += btnOpen.value;
                if (screen.value.length > 8) {
                    screen.scrollLeft += 30;
                }
                break;
            case "/":
                screen.value += btnOpen.value;
                if (screen.value.length > 8) {
                    screen.scrollLeft += 30;
                }
                break;
            case "*":
                screen.value += btnOpen.value;
                if (screen.value.length > 8) {
                    screen.scrollLeft += 30;
                }
                break;
            case "+":
                screen.value += btnOpen.value;
                if (screen.value.length > 8) {
                    screen.scrollLeft += 30;
                }
            case "":
                screen.value += btnOpen.value;
                break;
            default:
                break;
        }

    })

    // CLOSE PARENTHESES
    btnClose.addEventListener("click", () => {
        if (screen.value.indexOf("(") === -1) return;
        const open = countChar(screen.value, "[(]");
        const close = countChar(screen.value, "[)]");
        if (open <= close) return;
        screen.value += btnClose.value
        if (screen.value.length > 8) {
            screen.scrollLeft += 30;
        }
    });
});




